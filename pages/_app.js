import "../styles/normalize.css";
import "../styles/fonts.css";
import "../styles/common.css";
import "../styles/landing.css";
import React , {useEffect} from "react";
import NavBar from "../components/common/navbar";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    window.addEventListener(
      "keydown",
      function (e) {
        if (e.key === "F8") {
          debugger;
        }
      },
      false
    );
  })
  return (
    <React.Fragment>
      <NavBar />
      <div className="app_container">
        <Component {...pageProps} />
      </div>
      <style jsx>
        {`
          @font-face {
            font-family: "Vazir UI Thin", sans-serif;
            src: url(/fonts/Vazir-Thin-UI.woff2) format("woff2"),
              url(/fonts/Vazir-Thin-UI.woff) format("woff"),
              url(/fonts/Vazir-Thin-UI.ttf) format("truetype");
          }

          @font-face {
            font-family: "Vazir UI Light", sans-serif;
            src: url(/fonts/Vazir-Light-UI.woff2) format("woff2"),
              url(/fonts/Vazir-Light-UI.woff) format("woff"),
              url(/fonts/Vazir-Light-UI.ttf) format("truetype");
          }

          @font-face {
            font-family: "Vazir UI", sans-serif;
            src: url("/fonts/Vazir-Regular-UI.woff2") format("woff2"),
              url("/fonts/Vazir-Regular-UI.woff") format("woff"),
              url("/fonts/Vazir-Regular-UI.ttf") format("truetype");
          }

          @font-face {
            font-family: "Vazir UI Medium", sans-serif;
            src: url(/fonts/Vazir-Medium-UI.woff2) format("woff2"),
              url(/fonts/Vazir-Medium-UI.woff) format("woff"),
              url(/fonts/Vazir-Medium-UI.ttf) format("truetype");
          }

          @font-face {
            font-family: "Vazir UI Bold", sans-serif;
            src: url(/fonts/Vazir-Bold-UI.woff2) format("woff2"),
              url(/fonts/Vazir-Bold-UI.woff) format("woff"),
              url(/fonts/Vazir-Bold-UI.ttf) format("truetype");
          }

          @font-face {
            font-family: "Vazir UI Black", sans-serif;
            src: url(/fonts/Vazir-Black-UI.woff2) format("woff2"),
              url(/fonts/Vazir-Black-UI.woff) format("woff"),
              url(/fonts/Vazir-Black-UI.ttf) format("truetype");
          }
        `}
      </style>
    </React.Fragment>
  );
}

export default MyApp;
