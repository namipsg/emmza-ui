const navLinks = [
  {
    title: 'جستجوی خانه',
    link: "#propertySearch",
    hasSubMenu: false,
    subMenu: null,
  },
  {
    title: "فرصت های سرمایه گذاری",
    link: "#investOpportunities",
    hasSubMenu: false,
    subMenu: null,
  },
  {
    title: "قیمت های روز",
    link: "#dailyPrices",
    hasSubMenu: false,
    subMenu: null,
  },
  {
    title: "پرسش و پاسخ",
    link: "#FAQ",
    hasSubMenu: false,
    subMenu: null,
  },
  {
    title: "همکاری",
    link: null,
    hasSubMenu: true,
    subMenu: [
      {
        title: "اخذ نمایندگی",
        link: "#representation",
      },
      {
        title: "فرصت های شغلی",
        link: "#vacancies",
      },
    ],
  },
];


const getNavLinks = () => {
    return [...navLinks]
}


module.exports = getNavLinks ;
