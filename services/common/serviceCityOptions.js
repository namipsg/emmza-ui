const cityOptions = [{
    label : 'کرج',
    value : 'karaj'
},{
    label : 'تهران',
    value : 'tehran'
},{
    label : 'مشهد',
    value : 'mashhad'
}]


const getCityOptions = () => {
    return [...cityOptions]
}

module.exports = getCityOptions